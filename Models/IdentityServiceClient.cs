﻿using StudioKit.Security.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Security.Entity.Identity.Models;

/// <summary>
/// A <see cref="ServiceClient"/> with specific roles.
/// </summary>
public class IdentityServiceClient : ServiceClient
{
	[InverseProperty("ServiceClient")]
	public ICollection<ServiceClientRole> Roles { get; set; }
}