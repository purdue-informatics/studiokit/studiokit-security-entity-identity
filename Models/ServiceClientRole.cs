﻿using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Security.Entity.Identity.Models;

/// <summary>
/// A role for an <see cref="IdentityServiceClient"/>. Connects to an <see cref="Role"/>.
/// </summary>
public class ServiceClientRole : IAuditable
{
	[StringLength(100)]
	public string ClientId { get; set; }

	[ForeignKey(nameof(ClientId))]
	public IdentityServiceClient ServiceClient { get; set; }

	public string RoleId { get; set; }

	[ForeignKey(nameof(RoleId))]
	public Role Role { get; set; }

	[Timestamp]
	[Required]
	public byte[] RowVersion { get; set; }

	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }

	public string LastUpdatedById { get; set; }
}